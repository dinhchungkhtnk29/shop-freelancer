FROM node:alpine
ENV APP_HOME /app
RUN apk update --no-cache && \
  apk add --no-cache vim bash
WORKDIR $APP_HOME
ADD package.json yarn.lock .eslintrc.json ./
ADD src src/
#RUN yarn install
# set timezone
RUN rm -f /etc/localtime
RUN ln -sf /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime
expose 3000