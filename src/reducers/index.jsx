import { combineReducers } from 'redux'
import home from './HomeReducer/HomeReducer'

const appReducer = combineReducers({
  home: home
})

export default appReducer
