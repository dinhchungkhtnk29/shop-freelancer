#!/bin/bash

git fetch origin && \
git checkout $1 && \

cp docker-compose.yml.prod docker-compose.yml && \
cp Dockerfile.prod Dockerfile && \

docker-compose build && \
docker-compose run --rm web yarn build && \
docker-compose up -d --force-recreate
